package com.example.week3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignupScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent login = new Intent(this,MainActivity.class);
        setContentView(R.layout.activity_signup_screen);
        Button singupBtn =findViewById(R.id.signup);
        singupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText firstName = findViewById(R.id.firstname);
                EditText lastName = findViewById(R.id.lastname);
                EditText email = findViewById(R.id.email);
                EditText password = findViewById(R.id.password);
                EditText rePassword = findViewById(R.id.repassword);
                EditText phone = findViewById(R.id.phone);
                registerUser(view, firstName,lastName,password,rePassword,phone,email,login);
            }
        });
    }

    private Boolean validateName(EditText text) {
        if (text == null ) {
            Toast.makeText(this,"Name can not be null",Toast.LENGTH_SHORT).show();
            return false;
        }
        else {
            return true;
        }
    }

    private Boolean validateEmail(EditText text) {

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (text == null ) {
            Toast.makeText(this,"Email can not be null",Toast.LENGTH_SHORT).show();
            return false;
        }
        String val = text.getText().toString();

        if (!val.matches(emailPattern)) {
            Toast.makeText(this,"Email must be valid",Toast.LENGTH_SHORT).show();
            return false;
        } else {

            return true;
        }
    }

    private Boolean validatePhoneNo(EditText text) {


        if (text == null) {
            Toast.makeText(this,"Phone can not be null",Toast.LENGTH_SHORT).show();

            return false;
        } else {

            return true;
        }
    }

    private Boolean validatePassword(EditText text) {
        String passwordVal = "^" +
                //"(?=.*[0-9])" +         //at least 1 digit
                //"(?=.*[a-z])" +         //at least 1 lower case letter
                //"(?=.*[A-Z])" +         //at least 1 upper case letter
                "(?=.*[a-zA-Z])" +      //any letter
                "(?=.*[@#$%^&+=])" +    //at least 1 special character
                "(?=\\S+$)" +           //no white spaces
                ".{8,}" +               //at least 4 characters
                "$";

        if (text == null) {
            Toast.makeText(this,"Password can not be null",Toast.LENGTH_SHORT).show();

            return false;
        }
        String val = text.getText().toString();

        if (!val.matches(passwordVal)) {
            Toast.makeText(this,"Password must be valid",Toast.LENGTH_SHORT).show();
            return false;
        } else {

            return true;
        }
    }
    private Boolean validaterePassword(EditText text,EditText pass) {

        if (text == null) {
            Toast.makeText(this,"Password can not be null",Toast.LENGTH_SHORT).show();

            return false;
        }
        String val = text.getText().toString();

        if (!val.matches(pass.getText().toString())) {
            Toast.makeText(this,"Password must match",Toast.LENGTH_SHORT).show();

            return false;
        } else {

            return true;
        }
    }

    public void registerUser(View view, EditText firstName,EditText lastName,EditText password,EditText rePassword,EditText phone,EditText email, Intent login) {

        if (!validateName(firstName) | !validateName(lastName) | !validatePassword(password) | !validaterePassword(rePassword,password) | !validatePhoneNo(phone) | !validateEmail(email)) {
            Toast.makeText(this,"All fields must be valid",Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            Toast.makeText(this,"Congratulations. Signed Up",Toast.LENGTH_SHORT).show();
            startActivity(login);
            finish();
        }
    }
}